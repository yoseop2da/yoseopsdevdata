//
//  AppDelegate.h
//  YoseopsDevData
//
//  Created by yoseop on 2014. 8. 29..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
