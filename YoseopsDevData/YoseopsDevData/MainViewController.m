//
//  ViewController.m
//  YoseopsDevData
//
//  Created by yoseop on 2014. 8. 29..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import "MainViewController.h"
#import "YSWebViewController.h"
#import "MBProgressHUD.h"

#define WebView 0
#define ToastPopUp 1

@interface MainViewController () <UITableViewDataSource, UITableViewDelegate>

@property NSArray *solutions;
@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self solutionsSetting];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"잡다한거" style:UIBarButtonItemStylePlain target:self action:@selector(openJapDaGuRiView)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)openJapDaGuRiView
{
    UIViewController *japDaGuRiViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"JapDaGuRiViewController"];
    [self.navigationController pushViewController:japDaGuRiViewController animated:YES];
}



-(void)solutionsSetting
{
    self.solutions = @[@"Web View",@"토스트 팝업",@"프로그래스 팝업" ,@"토스트 팝업 중지"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.solutions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    cell.textLabel.text = self.solutions[indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case WebView:
        {
            YSWebViewController *outSideURLViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OutSideURLViewController"];
            outSideURLViewController.urlString = @"naver.com";
            [self.navigationController pushViewController:outSideURLViewController animated:YES];
        }
            break;
        case ToastPopUp:
        {
            NSString *toastString = @"토스트 팝업입니다.(2초간)";
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[[[UIApplication sharedApplication] windows] firstObject] animated:YES];
            
            // Configure for text only and offset down
            hud.mode = MBProgressHUDModeText;
            hud.labelText = toastString;
            hud.labelFont = [UIFont systemFontOfSize:11];
            hud.margin = 10.f;
            hud.yOffset = 150.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud setUserInteractionEnabled:NO];
            
            [hud hide:YES afterDelay:2];
        }
            break;
            
        case 2:
        {
            // 2초간 프로그래스 팝업 띄우기.
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [hud hide:YES afterDelay:2];
        }
            break;
            
        case 3:
        {
            // 프로그래스 팝업 종료 명령어
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
            break;
            
        case 4:
        {

        }
            break;
        case 5:
        {
            
        }
            break;
        case 6:
        {
            
        }
            break;
        case 7:
        {
            
        }
            break;
        case 8:
        {
            
        }
            break;
        case 9:
        {
            
        }
            break;
        case 10:
        {
            
        }
            break;
            
            
        default:
            break;
    }
    
}

@end
