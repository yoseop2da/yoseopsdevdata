#import <UIKit/UIKit.h>

@interface YSWebViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;

// 필수적으로 넣어줘야함.
@property (strong, nonatomic) NSString * urlString;

@end