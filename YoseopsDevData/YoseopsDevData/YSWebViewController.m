#import "YSWebViewController.h"

@interface YSWebViewController () <UIWebViewDelegate>

@end

@implementation YSWebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Web View";
}

-(void)viewDidLayoutSubviews
{
    if ([self.urlString hasPrefix:@"http://"]) {
        
        NSString * urlString = self.urlString;
        urlString = [urlString stringByAddingPercentEscapesUsingEncoding:(NSStringEncoding)NSUTF8StringEncoding];

        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",urlString]]]];

    }
    else{
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",self.urlString]]]];
    }
    
    [self.webView setScalesPageToFit:YES];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - WebView Delegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"webView : %@",webView);
    NSLog(@"request : %@",request);
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    
}

@end
