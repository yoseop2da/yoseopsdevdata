//
//  JapDaGuRiViewController.m
//  YoseopsDevData
//
//  Created by yoseop on 2014. 8. 29..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import "JapDaGuRiViewController.h"
#import "MarqueelLabel/MarqueeLabel.h"

@interface JapDaGuRiViewController ()
@property (weak, nonatomic) IBOutlet UIButton *marqueeButton;
@property (weak, nonatomic) IBOutlet MarqueeLabel *customLabel;
@end

@implementation JapDaGuRiViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"잡다구리뷰";
    
    [self marQueeLabelSetting];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)marqueeButtonTouched:(UIButton *)sender {
    if(self.customLabel.isPaused){
        [self.marqueeButton setTitle:@"정지" forState:UIControlStateNormal];
        [self.customLabel unpauseLabel];
    }else{
        [self.marqueeButton setTitle:@"시작" forState:UIControlStateNormal];
        [self.customLabel pauseLabel];
    }
    
}

- (void)marQueeLabelSetting
{
    self.customLabel.marqueeType = MLContinuous;
    self.customLabel.animationDelay = 0;
    self.customLabel.animationCurve = UIViewAnimationOptionCurveLinear;
    self.customLabel.continuousMarqueeExtraBuffer = 50.0f;
    self.customLabel.numberOfLines = 1;
    self.customLabel.opaque = NO;
    self.customLabel.enabled = YES;
    self.customLabel.shadowOffset = CGSizeMake(0.0, -1.0);
    self.customLabel.textAlignment = NSTextAlignmentLeft;
    self.customLabel.text = @"이곳에 내용을 채워주세요~ 내용이 순식간에 지나갑니당.";
    [self.customLabel setFont:[UIFont systemFontOfSize:11.0f]];
    self.customLabel.textColor = [UIColor lightGrayColor];
    [self.customLabel setBackgroundColor:[UIColor clearColor]];
    [self.customLabel beginScroll];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
